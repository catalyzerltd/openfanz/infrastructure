terraform {

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.53.0"
    }

    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.4.1"
    }

    helm = {
      source = "hashicorp/helm"
      version = "2.2.0"
    }
  }
}

// Setup provider (AWS)
provider aws {
  region  = var.region
}

// Setup provider (K8)
provider kubernetes {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.cluster.token
}