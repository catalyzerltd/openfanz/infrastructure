// Data
data "aws_eks_cluster" "cluster" {
  name = module.aws-cluster.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.aws-cluster.cluster_id
}

// Resources
resource "aws_security_group" "api_worker_group_one" {
  name_prefix = "worker_group_mgmt_one"
  vpc_id      = module.aws-vpc.vpc_id

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = [
      "10.0.0.0/8",
    ]
  }
}

resource "aws_security_group" "api_all_worker" {
  name_prefix = "all_worker_management"
  vpc_id      = module.aws-vpc.vpc_id

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = [
      "10.0.0.0/8",
      "172.16.0.0/12",
      "192.168.0.0/16",
    ]
  }
}

// Module
module "aws-cluster" {
  source       = "terraform-aws-modules/eks/aws"
  cluster_name    = var.cluster_name
  cluster_version = "1.17"
  subnets         = module.aws-vpc.private_subnets
  version = "12.2.0"
  cluster_create_timeout = "1h"
  cluster_endpoint_private_access = true

  vpc_id = module.aws-vpc.vpc_id

  worker_groups = [
    {
      name                          = "${var.cluster_name}-worker-group-1"
      instance_type                 = "t2.small"
      additional_userdata           = "echo foo bar"
      asg_desired_capacity          = 1
      additional_security_group_ids = [
        aws_security_group.api_worker_group_one.id
      ]
    },
  ]

  worker_additional_security_group_ids = [
    aws_security_group.api_all_worker.id
  ]
  map_roles                            = var.map_roles
  map_users                            = var.map_users
  map_accounts                         = var.map_accounts
}